# How to deploy the containerized API successfully

## Things to note / Tech stack
    * Infrastructure is provisioned using Terraform
    * The server is bookstraped and deployed using Ansible
    * Server sits behind an Nginx proxy/load balancer
    * The application, Nginx and MongoDB are running within a single docker compose instance
    * API checker script is written in Go/Golang programming language

## Live url
        * http://ec2-16-171-13-208.eu-north-1.compute.amazonaws.com/api

## How to deploy the project
### prerequsites
        * you need to have both Terraform and Ansible installed on your local machine
### commands to run within the console

        * 1] export AWS_ACCESS_KEY_ID="the-access-key-provided"
        * 2] export AWS_SECRET_ACCESS_KEY="the-secret-key-provided"
        * 3] terraform init
        * 4] terraform deploy

        * done, server's up. copy the IP address into the browser i.e http://ip-address

### PS: ansible-playbook deployment is triggered from within terraform

## Checker script
### To run locally type
    * go run api-checker-script.go
### To deploy
    * Deploy the Golang file on a lambda serverless function
    * Use a scheduled cloudwatch agent (free) or https://console.cron-job.org (free) to schedule the calling of the api endpoint

