# !/bin/bash

# # ssh into server using the .pem file {change the IP address to that of the ec2 instance}
# sudo ssh -i terraform-private-key.pem ubuntu@11.11111.11111.11

# # update package manager
# sudo apt update

# # install git
# sudo apt install git

# # pull code from gitlab
# git pull gitlab.repo.com

# # install docker & docker-compose
