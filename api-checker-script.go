package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

// The provided API does not have a means to check for version
// so I'm basically hitting the get events route...
// if the API is up & running, then status code would be 200

const (
	base_url  = "http://ec2-16-171-13-208.eu-north-1.compute.amazonaws.com"
	sub_url   = "/api/events/get-events"
	total_url = base_url + sub_url
)

type responseObject struct {
	result  string
	message string
	payload any
}

func main() {
	response, err := http.Get(total_url)
	if err != nil {
		log.Fatal(err)
	}

	if response.StatusCode == 200 {
		fmt.Println("API running successfully, status code", response.StatusCode)
		os.Exit(1)
	}

	fmt.Println("API not running, status code", response.StatusCode)
	os.Exit(0)
}
